import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';

function configureStore() {
    return createStore(
        function (state = {}, action) {
            switch(action.type) {
                case 'UPDATE_FIELDS':
                    state = Object.assign({}, state, action.payload);
                    break;
                default:
                    break;
            }

            return state;
        },
        applyMiddleware(reduxImmutableStateInvariant())
    );
}

export default configureStore();