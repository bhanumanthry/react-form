import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { withStyles, createStyleSheet } from 'material-ui/styles';
import Typography from 'material-ui/Typography'
import * as Colors from 'material-ui/colors';
import moment from 'moment';


const styleSheet = createStyleSheet('TextFields', theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
    },
    genderButton: {
        minWidth: '8rem',
        border: '1px solid',
        borderColor: Colors.blue
    },
    formField: {
        marginBottom: '2rem'
    },
    customSelectField: {
        width: '80%',
        height: '2.5rem',
        background: 'white',
    },
    dobStyle: {
        height: '2rem',
        width: '100%',
        textAlign: 'center',
        outline: 'none',
        fontSize: '1rem',
        color: 'rgba(0, 0, 0, 0.87)'
    }
}));

let selectedGenderStyle = {
    background: '#3f51b5',
    color: 'white'
};

let dob;

function lpad(value, padding) {
    var zeroes = new Array(padding+1).join("0");
    return (zeroes + value).slice(-padding);
}

class Form2 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dob: this.props.fieldValues.dob,
            dobDate: '',
            dobMonth: '',
            dobYear: '',
            aboutus: this.props.fieldValues.aboutus,
            gender: this.props.fieldValues.gender,
            dobErr: false,
            dobLabel: 'DATE OF BIRTH',
            genderErr: false,
            genderLabel: 'GENDER',
            aboutusErr: false,
            aboutusLabel: 'WHERE DID YOU HEAR ABOUT US?',
            maleSelected: {},
            femaleSelected: {},
            unspecifiedSelected: {}
        }
    }

    componentDidMount() {
        this.selectGender(this.props.fieldValues.gender);

    }

    isValid() {
        let gender = this.state.gender,
            aboutus = this.state.aboutus;

        dob = lpad(this.state.dobDate, 2) + '-' + lpad(this.state.dobMonth, 2) + '-' + this.state.dobYear;

        // if everything is valid
        this.setState({
            dobErr: false,
            dobLabel: 'DATE OF BIRTH',
            genderErr: false,
            genderLabel: 'GENDER',
            aboutusErr: false,
            aboutusLabel: 'WHERE DID YOU HEAR ABOUT US?'
        });

        if(!dob) {
            this.setState({
                dobErr: true,
                dobLabel: 'DATE OF BIRTH IS REQUIRED'
            });

            return false;
        }

        if(!moment(dob, 'DD-MM-YYYY', true).isValid()) {
            this.setState({
                dobErr: true,
                dobLabel: 'INVALID DATE'
            });

            return false;
        }

        let dateDiff = moment().diff(moment(dob,'DD-MM-YYYY'), 'years');


        if(dateDiff < 18) {
            this.setState({
                dobErr: true,
                dobLabel: 'YOU ARE LESSER THAN 18 YEARS'
            });

            return false;
        }

        if(!gender) {
            this.setState({
                genderErr: true,
                genderLabel: 'GENDER IS REQUIRED'
            });

            return false;
        }


        // returns true if everyting is ok
        return true;
    }

    saveAndContinue() {
        if(!this.isValid()) {
            return;
        }

        this.props.saveValues({
            dob: dob || this.props.fieldValues.dob,
            gender: this.state.gender || this.props.fieldValues.gender,
            aboutus: this.state.aboutus || this.props.fieldValues.aboutus,
        });

        this.props.nextStep();
    }

    selectGender(g) {
        switch(g) {
            case 'male':
                this.setState({
                    gender: g,
                    maleSelected: selectedGenderStyle,
                    femaleSelected: {},
                    unspecifiedSelected: {}
                });
                break;
            case 'female':
                this.setState({
                    gender: g,
                    maleSelected: {},
                    femaleSelected: selectedGenderStyle,
                    unspecifiedSelected: {}
                });
                break;
            case 'unspecified':
                this.setState({
                    gender: g,
                    maleSelected: {},
                    femaleSelected: {},
                    unspecifiedSelected: selectedGenderStyle
                });
                break;
        }
    }

    render() {
        const classes = this.props.classes;
        return (
            <div>
                <div style={{padding: '1rem', textAlign: 'center'}}>
                    <div className={classes.formField}>
                        <Typography style={{
                            paddingBottom: '0.5rem',
                            color: this.state.dobErr ? 'red' : 'rgba(0, 0, 0, 0.87)'
                        }}>{this.state.dobLabel}</Typography>
                        <div style={{display: 'flex', padding: '0 0.7rem'}}>
                            <input type="number" placeholder="DD" className={classes.dobStyle}
                                   step="1"
                                   min="1" max="31"
                                   maxLength={2}
                                   onChange={e => {
                                       this.setState({
                                           dobDate: e.target.value
                                       });
                                   }}
                            />

                            <input type="number" placeholder="MM" className={classes.dobStyle}
                                   step="1"
                                   min="1" max="12"
                                   maxLength="2"
                                   onChange={e => {
                                       this.setState({
                                           dobMonth: e.target.value
                                       });
                                   }}
                            />

                            <input type="number" placeholder="YYYY" className={classes.dobStyle}
                                   step="1"
                                   maxLength={4}
                                   min="1950" max="2017"
                                   onChange={e => {
                                       this.setState({
                                           dobYear: e.target.value
                                       });
                                   }}
                            />
                        </div>

                    </div>

                    <div className={classes.formField}>
                        <Typography style={{marginBottom: '1rem',
                                    color: this.state.genderErr ? 'red' : 'rgba(0, 0, 0, 0.87)'}}
                        >{this.state.genderLabel}</Typography>
                        <Button color="primary" className={classes.genderButton}
                                style={this.state.maleSelected}
                                onClick={() => this.selectGender('male')}
                        >MALE</Button>

                        <Button color="primary" className={classes.genderButton}
                                style={this.state.femaleSelected}
                                onClick={() => this.selectGender('female')}
                        >FEMALE</Button>

                        <Button color="primary" className={classes.genderButton}
                                style={this.state.unspecifiedSelected}
                                onClick={() => this.selectGender('unspecified')}
                        >UNSPECIFIED</Button>
                    </div>

                    <div className={classes.formField}>
                        <Typography style={{marginBottom: '1rem'}}>WHERE DID YOU HEAR ABOUT US?</Typography>
                        <select defaultValue={this.props.fieldValues.aboutus} className={classes.customSelectField}
                                onChange={e => {
                                    this.setState({
                                        aboutus: e.target.value
                                    });
                                }}
                        >
                            <option value={null}>&nbsp;</option>
                            <option value={'advertisement'}>Advertisement</option>
                            <option value={'website'}>Website</option>
                            <option value={'tv'}>TV</option>
                        </select>
                    </div>

                </div>

                <footer style={{borderTop: '1px solid #dedede'}}>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <Button color="default" className={classes.button}
                                onClick={() => {
                                    this.props.saveValues({
                                        dob: this.state.dob || this.props.fieldValues.dob,
                                        aboutus: this.state.aboutus || this.props.fieldValues.aboutus,
                                        gender: this.state.gender || this.props.fieldValues.gender
                                    });
                                    this.props.previousStep()
                                }}
                        >
                            Previous
                        </Button>

                        <Button color="primary" className={classes.button}
                                onClick={() => this.saveAndContinue()}
                        >
                            Next
                        </Button>
                    </div>
                </footer>
            </div>

        );
    }
}

Form2.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styleSheet)(Form2);
