import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { withStyles, createStyleSheet } from 'material-ui/styles';


const styleSheet = createStyleSheet('TextFields', theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
    }
}));

class Form1 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: this.props.fieldValues.email,
            password: this.props.fieldValues.email,
            confirmPassword: this.props.fieldValues.email,
            emailErr: false,
            emailLabel: 'EMAIL',
            passwordErr: false,
            passwordLabel: 'PASSWORD',
            confirmPasswordErr: false,
            confirmPasswordLabel: 'CONFIRM PASSWORD'
        };
    }

    isValid() {
        let email = this.state.email,
            password = this.state.password,
            confirmPassword = this.state.confirmPassword;

        // if everything is valid
        this.setState({
            emailErr: false,
            emailLabel: 'EMAIL',
            passwordErr: false,
            passwordLabel: 'PASSWORD',
            confirmPasswordLabel: 'CONFIRM PASSWORD'
        });

        function validEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        if(!email) {
            this.setState({
                emailErr: true,
                emailLabel: 'Email is required'
            });

            return false;
        }

        if(!validEmail(email)) {
            this.setState({
                emailErr: true,
                emailLabel: 'INVALID EMAIL'
            });

            return false;
        }

        if(!password) {
            this.setState({
                passwordErr: true,
                passwordLabel: 'PASSWORD IS REQUIRED'
            });

            return false;
        }

        if(!confirmPassword) {
            this.setState({
                confirmPasswordErr: true,
                confirmPasswordLabel: 'PLEASE CONFIRM YOUR PASSWORD'
            });

            return false;
        }

        if(password !== confirmPassword) {
            this.setState({
                passwordErr: true,
                passwordLabel: 'PASSWORDS MISMATCH',
                confirmPasswordErr: true,
                confirmPasswordLabel: 'PASSWORDS MISMATCH'
            });

            return false;
        }

        // returns true if everyting is ok
        return true;
    }

    saveAndContinue() {
        if(!this.isValid()) {
            return;
        }

        this.props.saveValues({
            email: this.state.email || this.props.fieldValues.email,
            password: this.state.password || this.props.fieldValues.password,
        });

        this.props.nextStep();
    }

    render() {
        const classes = this.props.classes;
        return (
            <div>
                <div style={{padding: '1rem'}}>
                    <TextField
                        error={this.state.emailErr}
                        ref={input => {this.email = input}}
                        id="name"
                        label={this.state.emailLabel}
                        className={classes.textField}
                        defaultValue={this.props.fieldValues.email}
                        onChange={event => this.setState({email: event.target.value })}
                        margin="normal"
                    />

                    <TextField
                        error={this.state.passwordErr}
                        id="name"
                        label={this.state.passwordLabel}
                        type="password"
                        className={classes.textField}
                        defaultValue={this.props.fieldValues.password}
                        onChange={event => this.setState({password: event.target.value})}
                        margin="normal"
                    />

                    <TextField
                        error={this.state.confirmPasswordErr}
                        id="name"
                        label={this.state.confirmPasswordLabel}
                        type="password"
                        className={classes.textField}
                        defaultValue={this.props.fieldValues.password}
                        onChange={event => this.setState({ confirmPassword: event.target.value })}
                        margin="normal"
                    />
                </div>

                <footer style={{borderTop: '1px solid #dedede'}}>
                    <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                        <Button color="primary" className={classes.button}
                                onClick={() => this.saveAndContinue()}
                        >
                            Next
                        </Button>
                    </div>
                </footer>
            </div>

        );
    }
}

Form1.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styleSheet)(Form1);
