import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { withStyles, createStyleSheet } from 'material-ui/styles';
import * as Colors from 'material-ui/colors';
import Grid from 'material-ui/Grid';


const styleSheet = createStyleSheet('TextFields', theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    button: {
        margin: theme.spacing.unit,
    }
}));

class Success extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const classes = this.props.classes;
        return (
            <Grid container gutter={0} style={{height: '30rem'}} align="center">
                <Grid item xs={12}>
                    <div style={{textAlign: 'center'}}>
                        <img src="/img/success-circle.png"
                             style={{width: '60%', height: 'auto'}}
                             alt=""
                        />

                        <br/>
                        <Button color="primary"
                                style={{border: '1px solid', borderColor: Colors.blue}}
                                onClick={() => this.props.logDetails()}
                        >Go to Dashboard</Button>
                    </div>
                </Grid>
            </Grid>
        );
    }
}

Success.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styleSheet)(Success);
