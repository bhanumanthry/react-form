import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from './configureStore';

store.dispatch({
    type: 'UPDATE_FIELDS',
    payload: {
        email: '',
        password: '',
        dob: null,
        gender: null,
        aboutus: 'website'
    }
});

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
