function update(updatedFields) {
    return {
        type: 'UPDATE_FIELDS',
        payload: updatedFields
    };
}

export default {
    update: update
};