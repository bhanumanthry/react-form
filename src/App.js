import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import formActions from './formActions';

import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import { LinearProgress } from 'material-ui/Progress';

import Form1 from './components/Form1';
import Form2 from './components/Form2';
import Success from './components/Success';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
        progressValue: 0
    };

    this.nextStep = this.nextStep.bind(this);
    this.previousStep = this.previousStep.bind(this);
    this.saveValues = this.saveValues.bind(this);
    this.logDetails = this.logDetails.bind(this);
  }

    saveValues(fields) {
        this.props.formActions.update(fields);
    }

  logDetails() {
      setTimeout(() => {
          console.log(this.props.fieldValues);
      },500);
  }

  nextStep() {
    this.setState({
      step: this.state.step + 1
    });
  }

  previousStep() {
    this.setState({
      step: this.state.step - 1
    });
  }

  renderStep() {
      if(this)
    switch (this.state.step) {
      case 1:
        return <Form1 fieldValues={this.props.fieldValues}
                      nextStep={this.nextStep}
                      saveValues={this.saveValues}
                />
      case 2:
        return <Form2 fieldValues={this.props.fieldValues}
                      nextStep={this.nextStep}
                      previousStep={this.previousStep}
                      saveValues={this.saveValues}
                />
      case 3:
        return <Success fieldValues={this.props.fieldValues}
                        logDetails={this.logDetails}
                />
    }
  }
    render() {
      let progressValue;
      switch(this.state.step) {
          case 1:
              progressValue = 33;
              break;
          case 2:
              progressValue = 66;
              break;
          case 3:
              progressValue = 100;
              break;
      }
        return (
            <Grid container justify="center"
                  align="center" style={{height: '90vh'}}
            >
                <Grid item xs={12} sm={7} md={5} lg={3}>
                    <Paper style={{minHeight: 'auto'}}>
                    <div style={{textAlign: 'center', padding: '1rem'}}>Signup</div>
                    <LinearProgress mode="determinate" value={progressValue} />
                    {this.renderStep()}
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        formActions: bindActionCreators(formActions, dispatch)
    }
}

function mapStateToProps(state) {
    return {
        fieldValues: state,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
